/* Line Equation in format of Ax + By = C */
struct line_equation {
	long a;
	long b;
	long c;
};

/* Get Line Equation from 2 points of line */
struct line_equation lineeq_from_points(int x1, int y1, int x2, int y2)
{
	struct line_equation lineeq = {
		(y1 - y2),
		(x2 - x1),
		(x1 * y2 - x2 * y1)
	};

	return lineeq;
}

int line_intersection(
		int x11, int y11, int x12, int y12,
		int x21, int y21, int x22, int y22,
		long * x, long * y
) {
	struct line_equation line1 = lineeq_from_points(x11, y11, x12, y12);
	struct line_equation line2 = lineeq_from_points(x21, y21, x22, y22);

	long d = line1.a * line2.b - line1.b * line2.a;
	if (d == 0) {
		return 1;
	}

	if (x) *x = (line1.b*line2.c-line2.b*line1.c)/d;
	if (y) *y = (line2.a*line1.c-line1.a*line2.c)/d;

	return 0;
}
