extern int TEXT;

extern int HORIZON;

extern float VANISH;
extern float SURFACE_WIDTH;

extern int VIEW_DISTANCE;
extern int ADD_RAYS;

extern int LINES;
extern int LINE_SPEED;

extern int PALM_TREES;
extern int PALM_DENSITY;

extern const int PALM_HEIGHT;

extern const int CURVE;

