// Show or hide information text
int TEXT = 1;

/* At which line surface will end */
int HORIZON = 460;

// How narrow end of the road will be
// Working values: from 0 to 0.8 inclusive, bigger values don't work
float VANISH = 0.7;
// How wide road will be
float SURFACE_WIDTH = 0.25;
// I don't really know anymore what this variables mean.
// If you have problems - just play with them
int VIEW_DISTANCE = 400;
// If using narrow roads, increase this value to draw more lines hear horizon
int ADD_RAYS = 3;
// Number of horizontal and vertical sections
int LINES = 21;

int LINE_SPEED = 30;

int PALM_TREES = 1; // Turn Palm Trees on or off
int PALM_DENSITY = 6; // How often palmtress will encounter

const int PALM_HEIGHT = 300; // How big is palm tree

const int CURVE = 0; // Road Curve

