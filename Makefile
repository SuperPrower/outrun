CFLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf
SOURCES = main.c text.c settings.c
DEBUG = -g

all: outrun

outrun:
	gcc -Wall $(DEBUG) $(CFLAGS) $(SOURCES) -o outrun

.PHONY: clean
clean:
	rm outrun
