#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "settings.h"

static SDL_Color color={255,255,255,SDL_ALPHA_OPAQUE};

static char van_info[64];
static char rwd_info[64];
static char vds_info[64];
static char ars_info[64];
static char hor_info[64];
static char lns_info[64];
static char lsp_info[64];
static char plm_info[64];

void show_status_text(SDL_Renderer * renderer, TTF_Font * font)
{

	snprintf(van_info, 64,
		"(up | down) Vanish point coefficient: %7.2f",
		VANISH
	);

	snprintf(rwd_info, 64,
		"(q | w) Surface width: %7.2f",
		SURFACE_WIDTH
	);

	snprintf(vds_info, 64,
		"(left | right) Diagonal vanish point: %d",
		VIEW_DISTANCE
	);

	snprintf(ars_info, 64,
		"( 1 | 2 ) Additional ray coefficient: %d",
		ADD_RAYS
	);

	snprintf(hor_info, 64,
		"( a | s ) Horizon height: %d",
		HORIZON
	);

	snprintf(lns_info, 64,
		"( [ | ] ) Lines: %d",
		LINES
	);

	snprintf(lsp_info, 64,
		"( - | + ) Line frames: %d",
		LINE_SPEED
	);


	if (PALM_TREES == 0)
		snprintf(plm_info, 64,
			"( P ) Palm trees: disabled"
		);
	else
		snprintf(plm_info, 64,
			"( P ) ( < | > ) Palm trees distance: %d",
			PALM_DENSITY
		);

	/* TEXT SURFACES */

	SDL_Surface * van_surf = TTF_RenderText_Solid(
		font,
		van_info,
		color
	);

	SDL_Surface * rwd_surf = TTF_RenderText_Solid(
		font,
		rwd_info,
		color
	);

	SDL_Surface * vds_surf = TTF_RenderText_Solid(
		font,
		vds_info,
		color
	);

	SDL_Surface * ars_surf = TTF_RenderText_Solid(
		font,
		ars_info,
		color
	);

	SDL_Surface * hor_surf = TTF_RenderText_Solid(
		font,
		hor_info,
		color
	);

	SDL_Surface * lns_surf = TTF_RenderText_Solid(
		font,
		lns_info,
		color
	);

	SDL_Surface * lsp_surf = TTF_RenderText_Solid(
		font,
		lsp_info,
		color
	);

	SDL_Surface * plm_surf = TTF_RenderText_Solid(
		font,
		plm_info,
		color
	);


	/* TEXT TEXTURES */

	SDL_Texture * van_tex = SDL_CreateTextureFromSurface(
		renderer, van_surf
	);


	SDL_Texture * rwd_tex = SDL_CreateTextureFromSurface(
		renderer, rwd_surf
	);


	SDL_Texture * vds_tex = SDL_CreateTextureFromSurface(
		renderer, vds_surf
	);


	SDL_Texture * ars_tex = SDL_CreateTextureFromSurface(
		renderer, ars_surf
	);

	SDL_Texture * hor_tex = SDL_CreateTextureFromSurface(
		renderer, hor_surf
	);

	SDL_Texture * lns_tex = SDL_CreateTextureFromSurface(
		renderer, lns_surf
	);

	SDL_Texture * lsp_tex = SDL_CreateTextureFromSurface(
		renderer, lsp_surf
	);

	SDL_Texture * plm_tex = SDL_CreateTextureFromSurface(
		renderer, plm_surf
	);


	SDL_Rect van_rect = {0, 0, 0, 0};
	SDL_Rect rwd_rect = {0, 40, 0, 0};
	SDL_Rect vds_rect = {0, 80, 0, 0};
	SDL_Rect ars_rect = {0, 120, 0, 0};
	SDL_Rect hor_rect = {0, 160, 0, 0};
	SDL_Rect lns_rect = {0, 200, 0, 0};
	SDL_Rect lsp_rect = {0, 240, 0, 0};
	SDL_Rect plm_rect = {0, 280, 0, 0};

	SDL_QueryTexture(van_tex, NULL, NULL, &van_rect.w, &van_rect.h);
	SDL_QueryTexture(rwd_tex, NULL, NULL, &rwd_rect.w, &rwd_rect.h);
	SDL_QueryTexture(vds_tex, NULL, NULL, &vds_rect.w, &vds_rect.h);
	SDL_QueryTexture(ars_tex, NULL, NULL, &ars_rect.w, &ars_rect.h);
	SDL_QueryTexture(hor_tex, NULL, NULL, &hor_rect.w, &hor_rect.h);
	SDL_QueryTexture(lns_tex, NULL, NULL, &lns_rect.w, &lns_rect.h);
	SDL_QueryTexture(lsp_tex, NULL, NULL, &lsp_rect.w, &lsp_rect.h);
	SDL_QueryTexture(plm_tex, NULL, NULL, &plm_rect.w, &plm_rect.h);

	SDL_RenderCopy(renderer, van_tex, NULL, &van_rect);
	SDL_RenderCopy(renderer, rwd_tex, NULL, &rwd_rect);
	SDL_RenderCopy(renderer, vds_tex, NULL, &vds_rect);
	SDL_RenderCopy(renderer, ars_tex, NULL, &ars_rect);
	SDL_RenderCopy(renderer, hor_tex, NULL, &hor_rect);
	SDL_RenderCopy(renderer, lns_tex, NULL, &lns_rect);
	SDL_RenderCopy(renderer, lsp_tex, NULL, &lsp_rect);
	SDL_RenderCopy(renderer, plm_tex, NULL, &plm_rect);

	SDL_FreeSurface(van_surf);
	SDL_FreeSurface(rwd_surf);
	SDL_FreeSurface(vds_surf);
	SDL_FreeSurface(ars_surf);
	SDL_FreeSurface(hor_surf);
	SDL_FreeSurface(lns_surf);
	SDL_FreeSurface(lsp_surf);
	SDL_FreeSurface(plm_surf);

	SDL_DestroyTexture(van_tex);
	SDL_DestroyTexture(rwd_tex);
	SDL_DestroyTexture(vds_tex);
	SDL_DestroyTexture(ars_tex);
	SDL_DestroyTexture(hor_tex);
	SDL_DestroyTexture(lns_tex);
	SDL_DestroyTexture(lsp_tex);
	SDL_DestroyTexture(plm_tex);
}
