#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "settings.h"
#include "line.c"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

#define SCREEN_FPS 90
#define SCREEN_TICKS_PER_FRAME (1000 / SCREEN_FPS)

void draw_lines(SDL_Renderer *, int, SDL_Texture *, int);

void quit(SDL_Window * window, SDL_Renderer * renderer);

void show_status_text(SDL_Renderer *, TTF_Font *);

int main()
{
	SDL_Window * window = NULL;
	SDL_Renderer * renderer = SDL_CreateRenderer(window, 0, 0);

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize!"
			"SDL_Error: %s\n", SDL_GetError());
		quit(window, renderer);
		return 1;
	}

	if(!IMG_Init(IMG_INIT_PNG)) {
		printf( "SDL_image could not initialize!"
		"SDL_image Error: %s\n", IMG_GetError());
		quit(window, renderer);
		return 1;
	}

	if (TTF_Init()) {
		printf("TTF_Init: %s\n", TTF_GetError());
	}

	TTF_Font* sans = TTF_OpenFont("sans.ttf", 24);
	if (!sans) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
	}

	int ret = SDL_CreateWindowAndRenderer(
		SCREEN_WIDTH, SCREEN_HEIGHT, 0,
		&window, &renderer
	);

	if (ret != 0) {
		printf("Window and/or renderer could not be created!"
			"SDL_Error: %s\n", SDL_GetError());

		quit(window, renderer);

		return 1;
	}

	// Load background texture
	SDL_Texture * background = IMG_LoadTexture(renderer, "sunset.png");
	SDL_Texture * palm = IMG_LoadTexture(renderer, "palmtree.png");


	SDL_Event e;
	SDL_bool done = SDL_FALSE;

	Uint32 clock = SDL_GetTicks(), elapsed = 0;

	int offset = 0;
	int palm_offset = 0;

	while (!done) {
		while (SDL_PollEvent( &e ) != 0) {
			switch(e.type) {
			case SDL_QUIT:
				done = SDL_TRUE;
				break;

			case SDL_KEYDOWN:
			switch (e.key.keysym.sym) {
				case SDLK_LEFT:  VIEW_DISTANCE -= 10; break;
				case SDLK_RIGHT: VIEW_DISTANCE += 10; break;
				case SDLK_UP:    VANISH += 0.05; break;
				case SDLK_DOWN:  VANISH -= 0.05; break;
				case SDLK_q:	 SURFACE_WIDTH -= 0.05; break;
				case SDLK_w:	 SURFACE_WIDTH += 0.05; break;
				case SDLK_1:	 ADD_RAYS -= 1; break;
				case SDLK_2:	 ADD_RAYS += 1; break;
				case SDLK_a:	 HORIZON -= 5; break;
				case SDLK_s:	 HORIZON += 5; break;
				case SDLK_t:	 TEXT = 1 - TEXT; break;
				case SDLK_p:	 PALM_TREES = 1 - PALM_TREES; break;

				case SDLK_RIGHTBRACKET:
					LINES += 1;
					break;
				case SDLK_LEFTBRACKET:
					LINES -= 1;
					break;

				case SDLK_EQUALS:
					LINE_SPEED += 1;
					offset = 0;
					break;
				case SDLK_MINUS:
					LINE_SPEED -= 1;
					offset = 0;
					break;

				case SDLK_COMMA: PALM_DENSITY -= 1; break;
				case SDLK_PERIOD: PALM_DENSITY += 1; break;
			}

			VIEW_DISTANCE 	= VIEW_DISTANCE <= 0 	? 10 	: VIEW_DISTANCE;
			VANISH 		= VANISH <= 0 		? 0.05 	: VANISH;
			SURFACE_WIDTH 	= SURFACE_WIDTH < -0.45	? -0.45	: SURFACE_WIDTH;
			ADD_RAYS 	= ADD_RAYS <= 0 	? 1 	: ADD_RAYS;
			HORIZON 	= HORIZON <= 0 		? 5 	: HORIZON;
			LINES 		= LINES <= 0 		? 1	: LINES;
			LINE_SPEED 	= LINE_SPEED <= 0 	? 1 	: LINE_SPEED;
			PALM_DENSITY    = PALM_DENSITY <= 0	? 1	: PALM_DENSITY;
			break;
			}
		}

		elapsed += SDL_GetTicks() - clock;
		clock = SDL_GetTicks();

		if (elapsed > SCREEN_TICKS_PER_FRAME) {
			elapsed -= SCREEN_TICKS_PER_FRAME;

			SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
			SDL_RenderClear(renderer);

			SDL_RenderCopy(renderer, background, NULL, NULL);

			draw_lines(renderer, offset++, palm, palm_offset);

			if (offset == LINE_SPEED) {
				palm_offset = (palm_offset + 1) % PALM_DENSITY;
				offset = 0;
			}

			if (TEXT) {
				show_status_text(renderer, sans);
			}


			SDL_RenderPresent(renderer);

		}
	}

	TTF_CloseFont(sans);
	SDL_DestroyTexture(background);
	SDL_DestroyTexture(palm);

	quit(window, renderer);

	return 0;
}

void quit(SDL_Window * window, SDL_Renderer * renderer)
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	//Quit SDL subsystems
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}

void draw_lines(SDL_Renderer * renderer, int offset, SDL_Texture * palm, int palm_offset)
{

	long BASE = 0 - SCREEN_WIDTH * SURFACE_WIDTH;
	long LIMT = SCREEN_WIDTH * (SURFACE_WIDTH + 1);

	int i, r;

	long x_dest = LIMT;
	long y_dest = SCREEN_HEIGHT + PALM_HEIGHT;

	long x_cross, y_cross;

	long x_center = SCREEN_WIDTH / 2;
	long y_vanish = HORIZON * VANISH;

	long step = (LIMT - BASE) / LINES;

	long x_edge_l, x_edge_r;

	/* Draw Horizon */
	SDL_SetRenderDrawColor(renderer, 50, 253, 253, SDL_ALPHA_OPAQUE);

	// Find cross of this line with "edges of road"
	line_intersection(
		x_center, y_vanish, BASE, y_dest,
		BASE, HORIZON, LIMT, HORIZON,
		&x_edge_l, NULL
	);

	line_intersection(
		x_center, y_vanish, LIMT, y_dest,
		BASE, HORIZON, LIMT, HORIZON,
		&x_edge_r, NULL
	);

	if (HORIZON > y_vanish) {
		SDL_RenderDrawLine(
			renderer,
			x_edge_l, HORIZON,
			x_edge_r, HORIZON
		);

		/* Highlight road edges */
		SDL_RenderDrawLine(
			renderer,
			x_edge_l, HORIZON,
			BASE, y_dest
		);

		SDL_RenderDrawLine(
			renderer,
			x_edge_r, HORIZON,
			LIMT, y_dest
		);
	} else {
		// If vanish point is lower that the horizon,
		// don't highlight the horizon and highlight only
		// seen part of the road
		SDL_RenderDrawLine(
			renderer,
			x_center, y_vanish,
			BASE, y_dest
		);

		SDL_RenderDrawLine(
			renderer,
			x_center, y_vanish,
			LIMT, y_dest
		);
	}

	SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
	for (i = 1, x_dest -= step; i < LINES; x_dest -= step, i++) {

		// Draw horizontal line
		r = line_intersection(
			x_center, y_vanish, x_dest, y_dest,
			BASE, HORIZON, LIMT, HORIZON,
			&x_cross, NULL
		);

		if (r) continue;

		if (y_vanish > HORIZON) {
			SDL_RenderDrawLine(
				renderer,
				x_center, y_vanish,
				x_dest, y_dest
			);
		} else {
			SDL_RenderDrawLine(
				renderer,
				x_cross, HORIZON,
				x_dest, y_dest
			);
		}


	}

	// Because often road rays is not enough to draw all horizontal
	// lines, we are going to draw several more imaginary lines
	long i_limit = (LIMT) * ADD_RAYS;
	int i_iter = (i_limit - BASE) / step;
	for (i = 0, x_dest = i_limit; i <= i_iter; x_dest -= step, i++) {

		// Draw vertial line
		int step_offset = step * offset / LINE_SPEED;

		r = line_intersection(
			x_center, y_vanish, x_dest - step_offset, y_dest,
			x_center + VIEW_DISTANCE, y_vanish, BASE, y_dest,
			NULL, &y_cross
		);

		if (r || y_cross <= HORIZON) continue;

		// Find cross of this line with "edges of road"
		r = line_intersection(
			x_center, y_vanish, BASE, y_dest,
			BASE, y_cross, LIMT, y_cross,
			&x_edge_l, NULL
		);

		r = r | line_intersection(
			x_center, y_vanish, LIMT, y_dest,
			BASE, y_cross, LIMT, y_cross,
			&x_edge_r, NULL
		);

		if (r) continue;

		SDL_RenderDrawLine(
			renderer,
			x_edge_l, y_cross,
			x_edge_r, y_cross
		);

		// Draw Palm Tree
		if (PALM_TREES && (i % PALM_DENSITY == palm_offset)) {
			int w, h;
			SDL_QueryTexture(palm, NULL, NULL, &w, &h);

			// Width to height ration (to resize)
			float k = (float) w / h;

			// How close ration (to make far palm trees smaller)
			float d = 1 - (((float) SCREEN_HEIGHT - y_cross) / ((float) SCREEN_HEIGHT - y_vanish));
			d = CURVE && d > 1 ? 1 : d;

			h = PALM_HEIGHT * d;
			w = k * h;

			int x_l = x_edge_l - (w / 2) + 10;
			int x_r = x_edge_r - (w / 2) - 10;

			int y_palm = y_cross - h;

			SDL_Rect l_palm = {
				x_l, y_palm,
				w, h
			};

			SDL_Rect r_palm = {
				x_r, y_palm,
				w, h
			};

			if (SDL_RenderCopyEx(renderer, palm, NULL, &l_palm, 0.0, NULL, SDL_FLIP_NONE) != 0) {
				printf("Left palmtree render error!"
					"SDL_Error: %s\n", SDL_GetError());
			}

			if (SDL_RenderCopyEx(renderer, palm, NULL, &r_palm, 0.0, NULL, SDL_FLIP_HORIZONTAL) != 0) {
				printf("Right palmtree render error!"
					"SDL_Error: %s\n", SDL_GetError());
			}
		}
	}
}

/* Code to draw pink road. Somewhat works, but not as good as I want.
int draw_the_road = (abs(i - LINES / 2) <= ROAD_WIDTH);
draw_the_road = draw_the_road || ((i > LINES / 2) && (i - LINES / 2) <= ROAD_WIDTH + 1 + LINES % 2);

if (draw_the_road) {
	int j;
	SDL_SetRenderDrawColor(renderer, 255, 0, 255, SDL_ALPHA_OPAQUE);
	for (j = 0; j < step; j++) {
		// Draw horizontal line
		r = line_intersection(
			x_center, y_vanish, x_dest + j, y_dest,
			BASE, HORIZON, LIMT, HORIZON,
			&x_cross, NULL
		);

		if (r) continue;

		if (y_vanish > HORIZON) {
			SDL_RenderDrawLine(
				renderer,
				x_center, y_vanish,
				x_dest + j, y_dest
			);
		} else {
			SDL_RenderDrawLine(
				renderer,
				x_cross, HORIZON,
				x_dest + j, y_dest
			);
		}

	}
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
}
*/
